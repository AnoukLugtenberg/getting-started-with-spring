package nl.bioinf.ahclugtenberg.gettingstarted;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
//import java.util.logging.Logger;

@Controller
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/{locale}/addStudents")
    public String showForm(Student student) {return "welcome";}


    @PostMapping(value = "/{locale}/addStudents")
    public String addNewStudent (@Valid Student student,
                                 BindingResult bindingResult,
                                 Model model) {

        Logger logger = LoggerFactory.getLogger(StudentController.class);

        if (bindingResult.hasErrors()) {
            return "{locale}/welcome";
        }
        
        Student s = new Student(student.getAge(), student.getName());
        logger.debug("--adding student--");

        studentRepository.save(new Student(student.getAge(), student.getName()));
        model.addAttribute("students", studentRepository.findAll());

        return "students";
    }
}
