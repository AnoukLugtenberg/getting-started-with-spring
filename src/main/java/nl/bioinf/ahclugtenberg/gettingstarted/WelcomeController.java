package nl.bioinf.ahclugtenberg.gettingstarted;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@Controller
public class WelcomeController {

    @RequestMapping(value = {"/", "/index", "/home", "/welcome"},
                    method = RequestMethod.GET)
    public String home(Model model, Locale locale) { return "redirect:" + locale.getLanguage() + "/home";}

    @RequestMapping(value = "/{locale}/home")
    public String welcome(@Valid @ModelAttribute("student") Student student,
                          BindingResult result, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy:ss");
        Date currentDate = new Date();
        model.addAttribute("serverTime", dateFormat.format(currentDate));

        List<Student> students = new ArrayList<Student>();
        Student Anouk = new Student();
        Anouk.setAge(1);
        Anouk.setName("Anouk");
        Student Kai = new Student();
        Kai.setAge(2);
        Kai.setName("Kai");
        students.add(Anouk);
        students.add(Kai);
        model.addAttribute("students", students);

        if (result.hasErrors()) {
            return "welcome";
        }
        return "welcome";
    }
}
