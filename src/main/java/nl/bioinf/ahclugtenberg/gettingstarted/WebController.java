package nl.bioinf.ahclugtenberg.gettingstarted;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Controller
public class WebController extends WebMvcConfigurerAdapter {

//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/results").setViewName("results");
//    }

    @GetMapping("/form")
    public String showForm(PersonForm personForm) {
        return "form";
    }

    @PostMapping("/form")
    public String checkPersonInfo(
            @ModelAttribute("personForm") PersonForm personForm,
            BindingResult bindingResult,
            Model model) {


        if (bindingResult.hasErrors()) {
            return "form";
        }

        model.addAttribute("name", personForm.getName());
        model.addAttribute("age", personForm.getAge());


        return "results";
    }
}
